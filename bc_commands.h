// used to enumerate the commands in the parser
#define	COMMAND_TRON			0
#define	COMMAND_TROFF			1
#define	COMMAND_ABOUT			2
#define COMMAND_HELP			3
#define COMMAND_EXIT			4
#define COMMAND_FIRST			5
#define COMMAND_LAST			6
#define COMMAND_NEXT			7
#define COMMAND_PREV			8
#define COMMAND_EXPORT		9

// command structure
struct dotComm {

	// command name
	char *Command;

	// command abbreviation
	char *Abbrev;

	// help line 1
	char *Help1;

	// help line 2
	char *Help2;

	// usage
	char *Usage;

	// ID number of the command
	int nCommand;
};

// define an array of command structures
struct dotComm dCommands[] = {

	{
		".tron",
		".tron",
		".tron",
		"Turn system trace ON",
		".tron",
		COMMAND_TRON
	},

	{
		".troff",
		".troff",
		".troff",
		"Turn system trace OFF",
		".troff",
		COMMAND_TROFF
	},

	{
		".about",
		".a",
		".about",
		"Show About dialog (also .a)",
		".about",
		COMMAND_ABOUT
	},

	{
		".help",
		".h",
		".help",
		"Display this text (also .h)",
		".help",
		COMMAND_HELP
	},

	{
		".exit",
		".x",
		".exit",
		"Exit from the program (also .x)",
		".exit",
		COMMAND_EXIT
	},

	{
		".first",
		".f",
		".first",
		"Show first book (also .f)",
		".first",
		COMMAND_FIRST
	},

	{
		".last",
		".l",
		".last",
		"Show last book (also .l)",
		".last",
		COMMAND_LAST
	},

	{
		".next",
		".n",
		".next",
		"Show next book (also .n)",
		".next",
		COMMAND_NEXT
	},

	{
		".prev",
		".p",
		".prev",
		"Show prev book (also .p)",
		".prev",
		COMMAND_PREV
	},

	{
		".export",
		".csv",
		".export",
		"Dump all books to CSV file (also .csv)",
		".export",
		COMMAND_EXPORT
	},

	{
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		(-1)
	}

};
