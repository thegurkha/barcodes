/*
 * gtk_functions.h
 *
 * Copyright 2013 Dave McKay <dmckay@btconnect.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <gtk/gtk.h>

#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include <sqlite3.h>

#include "bc_defines.h"
#include "bc_headers.h"

G_MODULE_EXPORT void on_window_destroy(GObject *object, BCGui *barcode_ui)
{
	// quit the GUI loop
	gtk_main_quit();

}	// end of on_window_destroy

G_MODULE_EXPORT void on_button1_clicked(GObject *object, BCGui *barcode_ui)
{
	const gchar *text;

	char *UserInput;

	// user has hit the post button or the enter key, retrieve the text from the text entry field
	text = gtk_entry_get_text(GTK_ENTRY(barcode_ui->TextField));

	// take a copy of the string and process it
	UserInput = strdup(text);

	ParseUserInput(barcode_ui, UserInput);

	// clear out the text entry field buffer
	gtk_entry_set_text(GTK_ENTRY(barcode_ui->TextField), "");

	free(UserInput);

}	// end of on_button1_clicked

G_MODULE_EXPORT void on_text_field_activate(GObject *object, BCGui *barcode_ui)
{
	// user hit enter, let's process their input
	on_button1_clicked(object, barcode_ui );

}	// end of on_text_field_button_press_event

G_MODULE_EXPORT void on_first_button_clicked(GObject *object, BCGui *barcode_ui)
{
	ShowFirstBook(barcode_ui);

}	// end of on_first_button_clicked

G_MODULE_EXPORT void on_prev_button_clicked(GObject *object, BCGui *barcode_ui)
{
	ShowPrevBook(barcode_ui);

}	// end of on_prev_button_clicked

G_MODULE_EXPORT void on_next_button_clicked(GObject *object, BCGui *barcode_ui)
{
	ShowNextBook(barcode_ui);

}	// end of on_next_button_clicked

G_MODULE_EXPORT void on_last_button_clicked(GObject *object, BCGui *barcode_ui)
{
	ShowLastBook(barcode_ui);

}	// end of on_last_button_clicked
