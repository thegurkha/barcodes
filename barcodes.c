/*
 * barcodes.c
 *
 * Copyright (C) 2018-22 David McKay
 *
 * Dave McKay dave@dpocompliance.co.uk
 *
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

// C standard header files
#include <gtk/gtk.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <locale.h>
#include <limits.h>
#include <time.h>
#include <stdbool.h>

// SQLite header file
#include <sqlite3.h>

// 3rd party header files
#include <curl/curl.h>
#include "tiny-json.h"

// our header files
#define MAIN
#include "bc_defines.h"
#include "bc_headers.h"
#include "bc_commands.h"

// database handle
sqlite3 *libraryDB = NULL;

enum  bColumns {ISBN, TITLE, SUBTITLE, PUBLISHED, PAGES, PUBLISHER, AUTHOR};
enum  lColumns {lTITLE, lSUBTITLE, lPUBLISHED, lPUBLISHER, lPAGES, lAUTHOR};
enum  ijColumns {ijTITLE, ijSUBTITLE, ijPUBLISHED, ijISBN, ijAUTHOR};

int main(int argc, char **argv)
{
	GtkBuilder 				*builder;
	BCGui							*barcode_ui;

	// allocate the memory needed by our structure
	barcode_ui = g_slice_new(BCGui);

	// init the gtk toolkit
	gtk_init(&argc, &argv);

	// intialise the SQLite library
	sqlite3_initialize();

	// get a new interface builder
	builder = gtk_builder_new();

	// read the GUI definitions from the XML file
	gtk_builder_add_from_file(builder, "barcodes.glade", NULL);

	// point the structure members to the UI elements we need to control
	barcode_ui->window = GTK_WIDGET (gtk_builder_get_object(builder, "window"));
	barcode_ui->TextField = GTK_ENTRY (gtk_builder_get_object(builder, "TextField"));
	barcode_ui->TextView = GTK_WIDGET (gtk_builder_get_object(builder, "textview1"));
	barcode_ui->Thumbnail = GTK_WIDGET (gtk_builder_get_object(builder, "bookThumbnail"));

	// connect the signal handlers
	gtk_builder_connect_signals(builder, barcode_ui);

	// release the builder memory
	g_object_unref(G_OBJECT (builder));

	// get the style settings from the CSS file
	GtkCssProvider *cssProvider = gtk_css_provider_new();
  gtk_css_provider_load_from_path(cssProvider, "themes.css", NULL);
  gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(cssProvider), GTK_STYLE_PROVIDER_PRIORITY_USER);

	// announce ourselves
	AppendText(barcode_ui, "Barcode Tests %d.%d.%d :: Compiled on %s at %s.",
			BC_VERSION_MAJOR, BC_VERSION_MINOR, BC_VERSION_BUILD, __DATE__, __TIME__);

	// start with trace off
	g_SYSTEM_TRACE = FALSE;

	// start off with thumbnail not found
	barcode_ui->bThumbnail = FALSE;

	// display the barcode icon
	gtk_image_set_from_file(GTK_IMAGE (barcode_ui->Thumbnail), "bc_icon.png");

	// display the window
	gtk_widget_show(barcode_ui->window);

	// open the database
	int nResponse = sqlite3_open_v2("library.sl3", &libraryDB, SQLITE_OPEN_READWRITE, NULL);

	// if it didn't open OK, shutdown and exit
	if (nResponse != SQLITE_OK) {
		printf("Couldn't open the library database [library.sl3]\n");
		sqlite3_close(libraryDB);
		exit (-1);
	}

	AppendText(barcode_ui, "Opened database: [library.sl3]");

	// blank line
	AppendText(barcode_ui, "");

	// read whatever is in the database
	Root=End=bCurrent=NULL;
	RetrieveAllISBN(barcode_ui);
	ShowFirstBook(barcode_ui);

	// enter the UI event handler loop
	gtk_main();

	// shut down and exit
	sqlite3_close(libraryDB);
	sqlite3_shutdown();

	// exit from the program
  exit (0);

} // end of main

int PerformBarcodeCurlRequest(BCGui *barcode_ui, char *szBarcode)
{
	CURL *curl_handle;
	CURLcode Res;
	int nResponse;

	char strURL[250];

	struct CurlBufferStruct CurlData;

	CurlData.CurlBuffer = malloc(1);  	// will be grown as needed by the realloc in WriteMemoryCallback()
	CurlData.size = 0;    							// no data at this point

	curl_global_init(CURL_GLOBAL_ALL);

	// init the curl session
	curl_handle = curl_easy_init();

	// form the web call
	sprintf(strURL, "https://www.googleapis.com/books/v1/volumes?q=isbn:%s", szBarcode);

	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, "%s", strURL);

	curl_easy_setopt(curl_handle, CURLOPT_URL, strURL);

	// send all data to this function
	curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

	// we pass our 'chunk' struct to the callback function
	curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *) &CurlData);

	// some servers don't like requests that are made without a user-agent field, so we provide one
	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	// get it!
	Res = curl_easy_perform(curl_handle);

	// check for errors
	if (Res != CURLE_OK) {
		// something bad happened, report it and return
		AppendText(barcode_ui, "curl_easy_perform() failed: %s", curl_easy_strerror(Res));
		return (1);
	}

	// extract the data
	nResponse = ParseBarcodeData(barcode_ui, &CurlData, szBarcode);

	// release the CurlData.CurlBuffer memory
	free(CurlData.CurlBuffer);

	// cleanup curl stuff
	curl_easy_cleanup(curl_handle);
	curl_global_cleanup();

	if (barcode_ui->bThumbnail) {
		if (PerformGetThumbnail(barcode_ui, szBarcode) == 0) {
			free(barcode_ui->ThumbnailURL);
			barcode_ui->bThumbnail = FALSE;
		}
		else
				gtk_image_set_from_file(GTK_IMAGE (barcode_ui->Thumbnail), "bc_icon.png");
	}
	else {
		gtk_image_set_from_file(GTK_IMAGE (barcode_ui->Thumbnail), "bc_icon.png");
	}

	// return
	return (nResponse);

}	// end of PerformBarcodeCurlRequest

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	// get the size of the new data
	size_t realsize = size * nmemb;

	// set a pointer to a CurlBufferStruct to map onto the buffer passed to us
	struct CurlBufferStruct *CurlMem = (struct CurlBufferStruct *) userp;

	// allocate more memory to the CurlBufferStruct
	CurlMem->CurlBuffer = realloc(CurlMem->CurlBuffer, CurlMem->size + realsize + 1);

	// complain if we can't get the memory
	if ( CurlMem->CurlBuffer == NULL) {

		// out of memory
		printf("WriteMemoryCallback: CurlMem - not enough memory (realloc returned NULL)\n");

		return ( 0 );
	}

	// copy the new data to the end of the buffer in the CurlBufferStruct
	memcpy(&(CurlMem->CurlBuffer[CurlMem->size]), contents, realsize);

	// store the new size
	CurlMem->size += realsize;

	// cap the buffer with a null byte
	CurlMem->CurlBuffer[ CurlMem->size ] = 0x00;

	// return the new buffer size
	return ( realsize );

}	// end of WriteMemoryCallback

void AppendText(BCGui *barcode_ui, char *NewString, ...)
{
	GtkTextBuffer *buffer;
	GtkTextIter iter;
	char text[8200];

	char TextBuffer[8192];
	va_list argptr;

	// if required compose the multiple arguments into a single line of text
	if (NewString != NULL) {
		va_start(argptr, NewString);
		vsprintf(TextBuffer, NewString, argptr);
		va_end(argptr);
	}

	// compose the line of text to append to the buffer
	sprintf(text, "%s\n", TextBuffer);

	// get a handle on the buffer from the text window
	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW (barcode_ui->TextView));

	// get the location of the last character + 1
	gtk_text_buffer_get_end_iter(buffer, &iter);

	// add a mark to the buffer, and place it at the end of the buffer
	GtkTextMark *gTextMark = gtk_text_mark_new(NULL, FALSE);
	gtk_text_buffer_add_mark(buffer, gTextMark, &iter);

	// display the line of text by adding it to the buffer
	gtk_text_buffer_insert_with_tags_by_name(buffer, &iter, text, -1, NULL, NULL);

	// scroll the window to the new mark (the end fo the buffer), then delete the mark
	gtk_text_view_scroll_to_mark(GTK_TEXT_VIEW (barcode_ui->TextView), gTextMark, 0.0, FALSE, 0.0, 0.0);
	gtk_text_buffer_delete_mark(buffer, gTextMark);

}	// end of AppendText

void PerformAboutCommand(BCGui *barcode_ui)
{
	char szSoftwareVersion[100];

	const char *Authors[] = {
		"Dave McKay <dmckay@btconnect.com>",
		NULL
	};

	const char *Catering[] = {
		"Pat MacKenzie <pcamackenzie@btconnect.com>",
		NULL
	};

	const char *Libraries[] = {
		"GCC https://gcc.gnu.org/",
		"GTK+ http://www.gtk.org/",
		"Glade https://glade.gnome.org/",
		"SQLite3 https://www.sqlite.org/",
		"tiny-json https://github.com/rafagafe/tiny-json/",
		NULL
	};

	sprintf( szSoftwareVersion, "Barcode Tests %d.%d.%d :: Compiled on %s at %s.",
			BC_VERSION_MAJOR, BC_VERSION_MINOR, BC_VERSION_BUILD, __DATE__, __TIME__);

	// set the icon
	GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file("bc_icon.png", NULL);

	// create the dialog box
	GtkWidget *AboutDialog = gtk_about_dialog_new();

	// set the data elements of the About box
	gtk_about_dialog_set_program_name( GTK_ABOUT_DIALOG(AboutDialog), "BC");
	gtk_about_dialog_set_version( GTK_ABOUT_DIALOG(AboutDialog), szSoftwareVersion);
	gtk_about_dialog_set_copyright( GTK_ABOUT_DIALOG(AboutDialog), "\u00A9 Dave McKay 2018");
	gtk_about_dialog_set_comments( GTK_ABOUT_DIALOG(AboutDialog), "Simple ISBN barcode scanning and retrieval.");
	gtk_about_dialog_set_website( GTK_ABOUT_DIALOG(AboutDialog), "http://www.ubuntujourneyman.com//");
	gtk_about_dialog_set_website_label( GTK_ABOUT_DIALOG(AboutDialog), "ubuntujourneyman.com");
	gtk_about_dialog_set_authors( GTK_ABOUT_DIALOG(AboutDialog), Authors);
	gtk_about_dialog_add_credit_section( GTK_ABOUT_DIALOG(AboutDialog), "Cups of Tea", Catering);
	gtk_about_dialog_add_credit_section( GTK_ABOUT_DIALOG(AboutDialog), "Libraries / Tools / APIs", Libraries);
	gtk_about_dialog_set_logo( GTK_ABOUT_DIALOG(AboutDialog), pixbuf);

	// don't need the handle to the icon anymore
	g_object_unref( pixbuf );
	pixbuf = NULL;

	// make the About dialog transient to the main window and a modal dialog
	gtk_window_set_modal((GtkWindow *) AboutDialog, TRUE);
	gtk_window_set_transient_for((GtkWindow *) AboutDialog, (GtkWindow *) barcode_ui->window);

	// display the about box
	gtk_dialog_run(GTK_DIALOG(AboutDialog));

	// don't need the dialog box anymore
	gtk_widget_destroy(AboutDialog);

}	// end of PerformAboutCommand

void ParseUserInput(BCGui *barcode_ui, char *InputString)
{
	char *cPtr, szSeps[]=" \n";

	int i, nCommand = (-1);

	// if it is not an empty string record it
	if ( strlen(InputString) < 1 )
			return;

	// get the first word of the user input
	cPtr = strtok(InputString, szSeps);

	if (cPtr == NULL)
			return;

	// see if it is in the list of commands
	for (i=0; dCommands[i].Command != NULL; i++) {

		// check abbreviations first
		if (strcmp(dCommands[i].Abbrev, cPtr) == 0) {
			// abbreviated command found
			nCommand = dCommands[i].nCommand;
			break;
		}
		// check the full name version if the abbrev didn't match
		else if (strcmp(dCommands[i].Command, cPtr) == 0) {
			// full command found
			nCommand = dCommands[i].nCommand;
			break;
		}
	}

	if (nCommand == (-1)) {
		AppendText(barcode_ui, "\n===============");
		AppendText(barcode_ui, "ISBN: %s", InputString);

		// do local catabase lookup
		if (PerformLocalLookup(barcode_ui, InputString) == FALSE) {
			PerformBarcodeCurlRequest(barcode_ui, InputString);
		}
		return;
	}

	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, "Command: %s detected", dCommands[i].Command);

	// perform the appropriate command action
	switch (nCommand) {

		// turn on the system trace functions
		case COMMAND_TRON:
			g_SYSTEM_TRACE = TRUE;
			AppendText(barcode_ui, "System Trace ON");
			break;

		// turn off the system trace functions
		case COMMAND_TROFF:
			g_SYSTEM_TRACE = FALSE;
			AppendText(barcode_ui, "System Trace OFF");
			break;

		case COMMAND_ABOUT:
			PerformAboutCommand(barcode_ui);
			break;

		case COMMAND_HELP:
			AppendText(barcode_ui, "\nBarcode Tests %d.%d.%d :: Compiled on %s at %s.",
					BC_VERSION_MAJOR, BC_VERSION_MINOR, BC_VERSION_BUILD, __DATE__, __TIME__);

			for (i=0; dCommands[i].Command != NULL; i++)
					AppendText(barcode_ui, "%-8s - %s", dCommands[i].Help1, dCommands[i].Help2);

			break;

		case COMMAND_FIRST:
			ShowFirstBook(barcode_ui);
			break;

		case COMMAND_LAST:
			ShowLastBook(barcode_ui);
			break;

		case COMMAND_NEXT:
			ShowNextBook(barcode_ui);
			break;

		case COMMAND_PREV:
			ShowPrevBook(barcode_ui);
			break;

		case COMMAND_EXPORT:
			DoExportCommand(barcode_ui);
			break;

		// exit from the program - perform the exit function
		case COMMAND_EXIT:
			gtk_widget_destroy(barcode_ui->window);
			break;

		default: break;
	}

}	// end of ParseUserInput

int ParseBarcodeData(BCGui *barcode_ui, struct CurlBufferStruct *CurlData, char * szBarcode)
{
	// tiny-json elements
	json_t curlFields[2048];
	json_t const *curlParent;

	int nNumberofItems = 0, nImageLinks = FALSE;
	char *strCurlBuffer = NULL;

	// book data
	char *bookTitle = NULL;
	char *bookSubtitle = NULL;
	char *bookPublisher = NULL;
	char *bookPublished = NULL;
	int bookPages;

	// display the number of retrieved bytes
	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, "Bytes retrieved: %lu", (long) CurlData->size );

	// copy the buffer into a string to work with it
	strCurlBuffer = strndup( CurlData->CurlBuffer, CurlData->size );

	// did we get the requested memory
	if ( strCurlBuffer == NULL ) {
		AppendText(barcode_ui, "Couldn't create strCurlBuffer");
		return (1);
	}

	// safety first - cap the string with a null byte
	strCurlBuffer[ CurlData->size ] = ASCII_NULL;

	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, "%s", strCurlBuffer);

	// parse the JSON curl data
	curlParent = json_create(strCurlBuffer, curlFields, 2048);

	// something bad happened, bomb out
	if ( curlParent == NULL) {
		AppendText(barcode_ui, "JSON: curlParent == NULL");
		free(strCurlBuffer);
		return (1);
	}

	//
	// find the totalItems name/value pair
	//
	json_t const* totalItems = json_getProperty(curlParent, "totalItems");
	// make sure it is valid
	if ((!totalItems) || (json_getType(totalItems) != JSON_INTEGER)) {
			AppendText(barcode_ui, "JSON: Couldn't get totalItems");
			free(strCurlBuffer);
			return (1);
	}
	// get the totalItems value
	nNumberofItems = json_getInteger(totalItems);

	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, "Total Items: %d", nNumberofItems);

	// if the book isn't in the database bomb out
	if (nNumberofItems == 0) {
		AppendText(barcode_ui, "JSON: No book details found: totalItems: %d", nNumberofItems);
		free(strCurlBuffer);
		return (1);
	}

	//
	// find the items array
	//
	json_t const* itemsArray = json_getProperty(curlParent, "items");
	// make sure it is valid
	if ((!itemsArray) || (json_getType(itemsArray) != JSON_ARRAY)) {
			AppendText(barcode_ui, "JSON: Couldn't locate the items array");
			free(strCurlBuffer);
			return (1);
	}

	//
	// go into the items array first child
	//
	json_t const* itemList = json_getChild(itemsArray);
	if (!itemList) {
		AppendText(barcode_ui, "JSON: No items array children");
		free(strCurlBuffer);
		return (1);
	}

	//
	// loop through the itemList array for the volumeInfo child
	//
	json_t const* itemChild;
	for (itemChild = json_getChild(itemList); itemChild != 0; itemChild = json_getSibling(itemChild)) {
			char const* itemName = json_getName(itemChild);
			if ((itemName) && (strcmp(itemName, "volumeInfo") == 0)) {
				break;
			}
	}

	//
	// go into the volumeInfo object
	//
	json_t const* volumeInfoList = json_getChild(itemChild);
	if (!volumeInfoList) {
		AppendText(barcode_ui, "JSON: No volumeInfo children");
		free(strCurlBuffer);
		return (1);
	}

	//
	// loop through the itemList array for the volumeInfo array
	//
	json_t const* itemVolumeInfo;
	for (itemVolumeInfo = json_getChild(itemChild); itemVolumeInfo != 0; itemVolumeInfo = json_getSibling(itemVolumeInfo)) {

		// get the name of this item
		char const* volumeInfoItemName = json_getName(itemVolumeInfo);

		// didn't get a name, ignore and loop round again
		if (!volumeInfoItemName)
				continue;

		// get the item value
		char const* itemValue  = json_getValue(itemVolumeInfo);

		// extract the values and display them
		if (strcmp(volumeInfoItemName, "title") == 0) {
			bookTitle = strdup(itemValue);
			AppendText(barcode_ui, "Title: %s", itemValue);
		}
		if (strcmp(volumeInfoItemName, "subtitle") == 0) {
			bookSubtitle = strdup(itemValue);
			AppendText(barcode_ui, "Subtitle: %s", itemValue);
		}
		else if (strcmp(volumeInfoItemName, "publisher") == 0) {
			bookPublisher = strdup(itemValue);
			AppendText(barcode_ui, "Publisher: %s", itemValue);
		}
		else if (strcmp(volumeInfoItemName, "publishedDate") == 0) {
			bookPublished = strdup(itemValue);
			AppendText(barcode_ui, "Date Published: %s", itemValue);
		}
		else if (strcmp(volumeInfoItemName, "pageCount") == 0) {
			bookPages = json_getInteger(itemVolumeInfo);
			AppendText(barcode_ui, "Pages: %d", bookPages);
		}
		else if (strcmp(volumeInfoItemName, "imageLinks") == 0) {
			// record the fact we found image links
			nImageLinks=TRUE;
		}
	}

	// add the book to the database
	if (bookTitle != NULL) {
		AddBookToDatabase(barcode_ui, szBarcode, bookTitle, bookSubtitle, bookPublisher, bookPublished, bookPages);
	}

	// don't need these any more
	free(bookTitle);
	free(bookSubtitle);
	free(bookPublisher);
	free(bookPublished);

	//
	// loop through again looking for authors
	//
	// AppendText(barcode_ui, "About to look for authors");
	for (itemVolumeInfo = json_getChild(itemChild); itemVolumeInfo != 0; itemVolumeInfo = json_getSibling(itemVolumeInfo)) {

		// get the name of this item
		char const* volumeInfoAuthors = json_getName(itemVolumeInfo);

		// if we didn't get a name or it wasn't 'authors' ignore it and loop round again
		if ((!volumeInfoAuthors) || (strcmp(volumeInfoAuthors, "authors") != 0))
				continue;

		// AppendText(barcode_ui, "Found authors array");
		// if we're here we found the authors array
		json_t const* author;
		// go into the array and then loop through the array
		for (author = json_getChild(itemVolumeInfo); author != 0; author = json_getSibling(author)) {
				// get the author name and display it, then loop in case there were more than one author
		    char const* authorName = json_getValue(author);
		    AppendText(barcode_ui, "Author: %s", authorName);

				// add the author to the database
				AddAuthorToDatabase(barcode_ui, szBarcode, (char *) authorName);
		}
	}

	if (nImageLinks) {

		// imageLinks were found, so loop through and get the thumbnail link
		for (itemVolumeInfo = json_getChild(itemChild); itemVolumeInfo != 0; itemVolumeInfo = json_getSibling(itemVolumeInfo)) {

			// get the name of this item
			char const* imageLinkItem = json_getName(itemVolumeInfo);

			// if we didn't get a name or it wasn't 'imageLinks' ignore it and loop round again
			if ((!imageLinkItem) || (strcmp(imageLinkItem, "imageLinks") != 0))
					continue;

			// if we're here we found the imageLinks array
			json_t const* thumbLink;
			// go into the array and then loop through the array
			for (thumbLink = json_getChild(itemVolumeInfo); thumbLink != 0; thumbLink = json_getSibling(thumbLink)) {
					// get the link name and save it if it is a'thumbnail'
			    char const* linkName = json_getName(thumbLink);
					if ((linkName) && (strcmp(linkName, "thumbnail") == 0)) {

						// re-use linkName to hold the URL
						linkName = json_getValue(thumbLink);

						if (g_SYSTEM_TRACE)
								AppendText(barcode_ui, "link: %s", linkName);

						barcode_ui->ThumbnailURL = strdup(linkName);
						barcode_ui->bThumbnail = TRUE;
				}
			}
		}
	}

	// purge ISBN linked list
	// reload ISBN linked list with new book in it
	// do list search to point bCurrent at new book
	DeleteAllISBN();
	RetrieveAllISBN(barcode_ui);
	DoListSearch(barcode_ui, szBarcode);

	// free the curl data buffer
	free(strCurlBuffer);

	return (0);

}	// end of ParseBarcodeData

int PerformGetThumbnail(BCGui *barcode_ui, char *szBarcode)
{
	CURL      *image;
  CURLcode  imgresult;
  FILE      *fp;
	char 			szFilename[100];

  image = curl_easy_init();

  if (image) {

      // create filename, open file
			sprintf(szFilename, "./thumbnails/%s.jpg", szBarcode);

			// if tron, report it
			if (g_SYSTEM_TRACE)
					AppendText(barcode_ui, "Thumbnail target: %s", szFilename);

			// open file
      fp = fopen(szFilename, "wb");

      if (fp == NULL) {
				// if tron, report it
				if (g_SYSTEM_TRACE)
						AppendText(barcode_ui, "Couldn't open: %s", szFilename);
				return (1);
			}

			// set up the curl options
      curl_easy_setopt(image, CURLOPT_URL, barcode_ui->ThumbnailURL);
      curl_easy_setopt(image, CURLOPT_WRITEFUNCTION, NULL);
      curl_easy_setopt(image, CURLOPT_WRITEDATA, fp);

      // Grab image
      imgresult = curl_easy_perform(image);

      if (imgresult) {
				if (g_SYSTEM_TRACE)
						AppendText(barcode_ui, "Couldn't retrieve thumbnail");
				return (1);
			}
  }

  // clean up curl resources
  curl_easy_cleanup(image);

  // Close the file
  fclose(fp);

	// display the image
	gtk_image_set_from_file(GTK_IMAGE (barcode_ui->Thumbnail), szFilename);

  return (0);

}	// end of PerformGetThumbnail

void AddBookToDatabase(BCGui *barcode_ui, char *bookISBN, char *bookTitle, char *bookSubtitle, char *bookPublisher, char *bookPublished, int bookPages)
{
	if (g_SYSTEM_TRACE) {
		AppendText(barcode_ui, "===");
		AppendText(barcode_ui, "Write: ISBN: %s", bookISBN);
		AppendText(barcode_ui, "Write: Title: %s", bookTitle);
		AppendText(barcode_ui, "Write: Subtitle: %s", bookSubtitle);
		AppendText(barcode_ui, "Write: Publisher: %s", bookPublisher);
		AppendText(barcode_ui, "Write: publishedDate: %s", bookPublished);
		AppendText(barcode_ui, "Write: Pages: %d", bookPages);
		AppendText(barcode_ui, "===");
	}

	char szSQLTemplate[]="INSERT INTO books VALUES (\"%s\", \"%s\", \"%s\", \"%s\", %d, \"%s\")";
	char szSQLString[2048];
	char *pError;
	int nResponse;

	// compose the SQL string
	sprintf(szSQLString, szSQLTemplate, bookISBN, bookTitle, (bookSubtitle == NULL) ? "" : bookSubtitle, bookPublished, bookPages, bookPublisher);

	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, szSQLString);

	// execute the SQL statement
	nResponse = sqlite3_exec(libraryDB, szSQLString, NULL, NULL, &pError);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK) {

		if (g_SYSTEM_TRACE)
				AppendText(barcode_ui, "Can't add book to library database");

		return;
	}
	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, "Added book to library database");

}	// end of AddBookToDatabase

void AddAuthorToDatabase(BCGui *barcode_ui, char *bookISBN, char *authorName)
{
	if (g_SYSTEM_TRACE) {
		AppendText(barcode_ui, "Write: ISBN: %s", bookISBN);
		AppendText(barcode_ui, "Write: Author: %s", authorName);
		AppendText(barcode_ui, "===");
	}

	char szSQLTemplate[]="INSERT INTO authors VALUES (\"%s\", \"%s\")";
	char szSQLString[512];
	char *pError;
	int nResponse;

	// compose the SQL string
	sprintf(szSQLString, szSQLTemplate, bookISBN, authorName);

	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, szSQLString);

	// execute the SQL statement
	nResponse = sqlite3_exec(libraryDB, szSQLString, NULL, NULL, &pError);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK) {

		if (g_SYSTEM_TRACE)
				AppendText(barcode_ui, "Can't add author to library database");

		return;
	}

	if (g_SYSTEM_TRACE)
			AppendText(barcode_ui, "Added author to library database");

}	// end of AddAuthorToDatabase

int PerformLocalLookup(BCGui *barcode_ui, char *szBarcode)
{	
	char szTemplate[300]="SELECT books.bookTitle, books.bookSubtitle, books.bookPublished, books.bookPublisher, books.bookPages, authors.authorName "
			"FROM books LEFT JOIN authors ON authors.ISBN=books.ISBN WHERE books.ISBN='%s' ORDER BY books.ISBN;";	

	char szSQLString[512]="";
	char szValueString[512]="";
	char szTempString[200]="";
	int nResponse, nAuthors=1;
	bool bFirst=true, bFound = false;

	sqlite3_stmt *stmt = NULL;

	// form the SQL string
	sprintf(szSQLString, szTemplate, szBarcode);

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(libraryDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return (FALSE);

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);
	
	// as long as we have a valid row in the record set
	while (nResponse == SQLITE_ROW) {

		// book is found
		bFound = true;

		if (bFirst==true) {

			// get data from fields of record set record and display them

			//
			// title
			//
			sprintf(szTempString, "%s", (char *) sqlite3_column_text(stmt, lTITLE));
			SanitizeField(szTempString);
			sprintf(szValueString, "    Title: %s", szTempString);
			AppendText(barcode_ui, szValueString);

			//
			// subtitle
			//
			sprintf(szTempString, "%s", (char *) sqlite3_column_text(stmt, lSUBTITLE));
			SanitizeField(szTempString);
			sprintf(szValueString, " Subtitle: %s", szTempString);
			AppendText(barcode_ui, szValueString);

			//
			// published
			//
			sprintf(szTempString, "%s", (char *) sqlite3_column_text(stmt, lPUBLISHED));
			SanitizeField(szTempString);
			sprintf(szValueString, "Published: %s", szTempString);
			AppendText(barcode_ui, szValueString);

			//
			// publisher
			//
			sprintf(szTempString, "%s", (char *) sqlite3_column_text(stmt, lPUBLISHER));
			SanitizeField(szTempString);
			sprintf(szValueString, "Publisher: %s", szTempString);
			AppendText(barcode_ui, szValueString);

			//
			// pages
			//
			sprintf(szTempString, "%s", (char *) sqlite3_column_text(stmt, lPAGES));
			SanitizeField(szTempString);
			sprintf(szValueString, "    Pages: %s", szTempString);
			AppendText(barcode_ui, szValueString);

			//
			// author
			//
			sprintf(szTempString, "%s", (char *) sqlite3_column_text(stmt, lAUTHOR));
			SanitizeField(szTempString);
			sprintf(szValueString, "   Author: %s", szTempString);
			AppendText(barcode_ui, szValueString);

			bFirst=false;
		}
		else {

			// there must be more than one author...
			++nAuthors;
			sprintf(szTempString, "%s", (char *) sqlite3_column_text(stmt, 5));
			SanitizeField(szTempString);
			sprintf(szValueString, "  Author%d: %s", nAuthors, szTempString);
			AppendText(barcode_ui, szValueString);
		}
		
		// step through the loop to the next record - if it fails there are no more authors
		nResponse = sqlite3_step(stmt);
	}

	sqlite3_finalize(stmt);

	if (bFound == false)
			return (false);

	if (bFound) {
		// locate the thumbnail

		sprintf(szValueString, "./thumbnails/%s.jpg", szBarcode);

		// try to find the JPG
		if (access(szValueString, F_OK) != 0) {

			// if we can't find the JPG look for a PNG
    	sprintf(szValueString, "./thumbnails/%s.png", szBarcode);

    	// if we can't find the PNG either, use the default barcode icon
			if (access(szValueString, F_OK) != 0) {
	    	sprintf(szValueString, "./thumbnails/bc_icon.png");
			}
		}

		if (g_SYSTEM_TRACE)
				AppendText(barcode_ui, szValueString);

		// display the thumbnail
		gtk_image_set_from_file(GTK_IMAGE (barcode_ui->Thumbnail), szValueString);

		// find the book in the list, set bCurrent to point at it
		DoListSearch(barcode_ui, szBarcode);
	}

	return (true);

}	// end of PerformLocalLookup

struct bLink *AddISBNToList(BCGui *barcode_ui, char *szISBN)
{
	struct bLink *fPtr;

	// no ISBNs in the list yet
	if (Root == NULL) {

		// get memory for the new ISBN structure
		fPtr = (struct bLink *) malloc(sizeof(struct bLink));

		// can't allocate memory
		if (fPtr == NULL) {
			AppendText(barcode_ui, "Error: Can't allocate memory for ISBN: %s in linked list.\n", szISBN);
			return (NULL);
		}

		// set this ISBN as the start of the linked list
		Root=End=fPtr;
	}
	// ISBNs exist, append new ISBN to the list
	else {
		// get memory for the new ISBN structure
		fPtr = (struct bLink *) malloc(sizeof(struct bLink));

		// can't allocate memory
		if (fPtr == NULL) {
			AppendText(barcode_ui, "Error: Can't allocate memory for ISBN: %s in linked list.\n", szISBN);
			return (NULL);
		}

		// point the hitherto last ISBN to the new last ISBN
		End->Next=fPtr;

		// point this link to the old last link
		fPtr->Prev=End;

		// this is now the end of the linked list
		End=fPtr;
	}

	// populate the ISBN field
	fPtr->ISBN=strdup(szISBN);

	// can't allocate memory
	if (fPtr->ISBN == NULL) {
		AppendText(barcode_ui, "Error: Can't allocate memory for fPtr->ISBN: %s in linked list.\n", szISBN);
		return (NULL);
	}

	// nothing comes after this link
	fPtr->Next=NULL;

	// return from function
	return (fPtr);

}	// end of AddISBNToList

void DoListWalk(BCGui *barcode_ui)
{
	struct bLink  *fPtr;

	// From the start of the linked list (root), display all ISBN
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {
		AppendText(barcode_ui, "%s %s", fPtr->ISBN, (bCurrent == fPtr) ? "**" : "");
	}

}	// end of DoListWalk

void DoListSearch(BCGui *barcode_ui, char *szBarcode)
{
	struct bLink *fPtr;
	int nFound = FALSE;

	// From the start of the linked list (root), search all ISBN
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		if (strcmp(fPtr->ISBN, szBarcode) == 0) {

			if ( g_SYSTEM_TRACE)
					AppendText(barcode_ui, "Found: %s", fPtr->ISBN);

			bCurrent = fPtr;
			nFound = TRUE;
			if ( g_SYSTEM_TRACE)
					AppendText(barcode_ui, "bCurrent: %s", bCurrent->ISBN);
			break;
		}
	}

	if (nFound == FALSE) {

		if (g_SYSTEM_TRACE)
				AppendText(barcode_ui, "Not found: %s", szBarcode);

		bCurrent=Root;
	}

}	// end of DoListSearch

void RetrieveAllISBN(BCGui *barcode_ui)
{
	char szSQLString[]="SELECT * FROM books";
	// char szSQLString[]="SELECT * FROM books ORDER BY bookTitle";
	char *pISBN;
	int nResponse;
	sqlite3_stmt *stmt = NULL;

	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(libraryDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return;

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	while (nResponse == SQLITE_ROW) {

		// take a copy of the three elements of the fact
		pISBN = (char*) sqlite3_column_text(stmt, ISBN);

		AddISBNToList(barcode_ui, pISBN);

		// check for the next record
		nResponse = sqlite3_step(stmt);
	}

	// close off the sql statement
	sqlite3_finalize(stmt);

	}	// end of RetrieveAllISBN

	void DeleteAllISBN(void)
	{
		struct bLink *fPtr = NULL, *nPtr = NULL;

		// From the start of the linked list (root), delete all ISBNs
		for (fPtr=Root; fPtr != NULL; fPtr=nPtr) {

			// get the pointer to the next ISBN before we delete this one
			nPtr = fPtr->Next;

			// free up the memory for the strdup'd string
			free(fPtr->ISBN);

			// null the pointers - safe but not strictly required
			fPtr->Next = NULL;
			fPtr->Prev = NULL;

			// free the memory for the structure itself
			free(fPtr);
		}

		// set start and end of the linked list to null
		// to indicate that the linked list is empty
		Root = End = NULL;

	}	// end of DeleteAllISBN

void ShowFirstBook(BCGui *barcode_ui)
{
	bCurrent=Root;

	AppendText(barcode_ui, "\n===============");
	AppendText(barcode_ui, "ISBN: %s", bCurrent->ISBN);

	PerformLocalLookup(barcode_ui, bCurrent->ISBN);

}	// end of ShowFirstBook

void ShowLastBook(BCGui *barcode_ui)
{
	bCurrent=End;

	AppendText(barcode_ui, "\n===============");
	AppendText(barcode_ui, "ISBN: %s", bCurrent->ISBN);

	PerformLocalLookup(barcode_ui, bCurrent->ISBN);

}	// end of ShowLastBook

void ShowNextBook(BCGui *barcode_ui)
{
	if (bCurrent->Next != NULL) {
		bCurrent=bCurrent->Next;

		AppendText(barcode_ui, "\n===============");
		AppendText(barcode_ui, "ISBN: %s", bCurrent->ISBN);

		PerformLocalLookup(barcode_ui, bCurrent->ISBN);
	}

}	// end of ShowNextBook

void ShowPrevBook(BCGui *barcode_ui)
{
	if (bCurrent->Prev != NULL) {
		bCurrent=bCurrent->Prev;

		AppendText(barcode_ui, "\n===============");
		AppendText(barcode_ui, "ISBN: %s", bCurrent->ISBN);

		PerformLocalLookup(barcode_ui, bCurrent->ISBN);
	}

}	// end of ShowPrevBook

void DoExportCommand(BCGui *barcode_ui)
{
	struct bLink  *fPtr;

	/* 
	CreateCSVRecord(barcode_ui, "9780198767251");
	return;
	 // */

	// From the start of the linked list (root)
	for (fPtr=Root; fPtr != NULL; fPtr=fPtr->Next) {

		// AppendText(barcode_ui, "%s", fPtr->ISBN);
		CreateCSVRecord(barcode_ui, fPtr->ISBN);
	}

}	// end of DoExportCommand

int CreateCSVRecord(BCGui *barcode_ui, char *szBarcode)
{
	char szTemplate[300]="SELECT books.bookTitle, books.bookSubtitle, books.bookPublished, books.ISBN, authors.authorName "
			"FROM books LEFT JOIN authors ON authors.ISBN=books.ISBN WHERE books.ISBN='%s' ORDER BY books.ISBN;";	

	char szSQLString[512]="";
	char szValueString[1024]="";
	char szAuthorString[100]="";
	int nResponse;

	bool bFound=false, bFirst=true;

	sqlite3_stmt *stmt = NULL;

	// AppendText(barcode_ui, szBarcode);

	// form the SQL string
	sprintf(szSQLString, szTemplate, szBarcode);
	
	// prepare the SQL statement
	nResponse = sqlite3_prepare_v2(libraryDB, szSQLString, -1, &stmt, NULL);

	// if not OK, bomb out
	if (nResponse != SQLITE_OK)
			return (false);

	// if OK, start to walk through the record set
	nResponse = sqlite3_step(stmt);

	// as long as we have a valid record set row
	while (nResponse == SQLITE_ROW) {

		// book is found
		bFound = true;

		if (bFirst==true) {

			// just in case there is no author listed...
			sprintf(szAuthorString, "%s", (char *) sqlite3_column_text(stmt, ijAUTHOR));
			SanitizeField(szAuthorString);

			// get book data
			sprintf(szValueString, "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"", 
				(char *) sqlite3_column_text(stmt, ijTITLE),
				(char *) sqlite3_column_text(stmt, ijSUBTITLE),
				(char *) sqlite3_column_text(stmt, ijPUBLISHED),
				(char *) sqlite3_column_text(stmt, ijISBN),
				szAuthorString
				// (char *) sqlite3_column_text(stmt, ijAUTHOR)
			);

			bFirst=false;
		}
		else {

			// there must be more than one author...
			sprintf(szAuthorString, ",\"%s\"", (char *) sqlite3_column_text(stmt, ijAUTHOR));
			strcat(szValueString, szAuthorString);
		}
		
		// step through the loop to the next record - if it fails there are no more authors
		nResponse = sqlite3_step(stmt);
	}

	AppendText(barcode_ui, szValueString);

	sqlite3_finalize(stmt);
	
	return (bFound) ? true : false;

}	// end of CreateCSVRecord

void SanitizeField(char *szFieldString)
{
	if (strcmp(szFieldString, "(null)")==0) {
		strcpy(szFieldString, "");
	}

}	// end of SanitizeField
