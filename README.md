# barcodes

bc - barcode experiments.

A tool to investigate reading a book's barcode and retrieving data from a Google web API

* Not meant to do much, just send a barcode string and get a response from a Google API.
* Stores the parsed data in a SQLite3 database. Subsequent scans of the barcode are searched for in the local database before calling the remote API.
* Just for fun and learning.

# Version
A tentative beta.

# How do I get set up?
Clone the repository and use:

	make

## Summary of set up

* Linux OS - EndeavourOS
* GCC compiler
* Sqlite3 library
* cURL library
* tiny-json library

## Configuration

Clone the tiny-json library from Github, and drop the tiny-json.c and .h files in your project folder

	https://github.com/rafagafe/tiny-json

## Dependencies

Libraries, Toolkits and Tools:

* gcc
* cUrl
* SQLite3
* tiny-json

## Who do I talk to?

* Repo owner or admin

* Dave McKay
	* dave@dpocompliance.co.uk
