#include <gtk/gtk.h>

#ifndef ALREADY_DEFINED
#define ALREADY_DEFINED

#define ASCII_NULL	      0x00

#define BC_VERSION_MAJOR  0
#define BC_VERSION_MINOR	4
#define BC_VERSION_BUILD	0

#if defined MAIN

// buffer for retrieved curl data
typedef struct CurlBufferStruct {

  char *CurlBuffer;
  size_t size;

} curlBuff;

// structure for barcodes UI
typedef struct _barcode_ui {

  GtkWidget	*window;      // pointer to main window
  GtkEntry *TextField;    // pointer to text entry field
  GtkWidget	*TextView;    // pointer to scrolling text TextView

  GtkWidget	*Thumbnail;    // pointer to GtkImage

  char *ThumbnailURL;     // pointer to thumbail URL (if present)
  int bThumbnail;

} BCGui;

// structure used in linked list of ISBN numbers
typedef struct bLink {

	char	*ISBN;

  struct bLink *Prev;
  struct bLink *Next;

} bLink;

// pointers to the start and end of the linked list
struct bLink *Root, *End, *bCurrent;

int g_SYSTEM_TRACE;

// ###################################################################################
#else

// buffer for retrieved curl data
typedef struct CurlBufferStruct {

  char *CurlBuffer;
  size_t size;

} curlBuff;

// structure for barcodes UI
typedef struct _barcode_ui {

  GtkWidget	*window;      // pointer to main window
  GtkEntry *TextField;    // pointer to text entry field
  GtkWidget	*TextView;    // pointer to scrolling text TextView

  GtkWidget	*Thumbnail;    // pointer to GtkImage

  char *ThumbnailURL;     // pointer to thumbail URL (if present)
  int bThumbnail;

} BCGui;

// structure used in linked list of ISBN numbers
typedef struct bLink {

	char	*ISBN;

  struct bLink *Prev;
  struct bLink *Next;

} bLink;

// extern BCGui BCGui;
extern bLink *Root, *End, *bCurrent;
extern int g_SYSTEM_TRACE;

// buffer for retrieved curl data
// extern CurlBufferStruct curlBuff;

#endif	// MAIN

#endif 	// ALREADY_DEFINED
